package sample;

import javafx.application.Application;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

import javafx.stage.Stage;

/**
 * Drawing tablet application.
 *
 * @author Rodrigo Calvo
 * @version 1.0
 */
public class Main extends Application {
    private ToolPane toolPane= new ToolPane();  //toolpane imported from ToolPane.java
    private Pane drawPane= new Pane(); // Drawiwng pane .
    private Pane mainPane = new Pane(); // Main pane, both toolpane and drawing pane are in here.
    private double x=0; // X variable to keep track of different shapes x positions
    private double y=0; // Y variable to keep track of different shapes y positions


    /**
     * Main class, calls user interface.
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        userInterface(primaryStage);

    }

    /**
     * User interface method, controls Panes and figures, keeps track of buttons, handles some of the events on the main pane.
     * @param primaryStage
     */
    private void userInterface(Stage primaryStage){
        primaryStage.setTitle("Drawing Tablet");
        mainPane.getChildren().addAll(toolPane,drawPane); // Adding toolpane and the drawpane to the main pane
        drawPane.setPrefSize(600,465); // Setting the size
        Scene scene = new Scene(mainPane, 600, 500); // Adding main pane to the scene
        primaryStage.setScene(scene); //Setting scene on stage
        Rectangle clip = new Rectangle(0,35,600,465); // creating clip for drawing pane
        drawPane.setClip(clip);
        
        //Sets x and y values to where the mouse was at the time when pressed and handles the different options of buttons
        mainPane.setOnMousePressed(event -> {
            x = event.getX();
            y=event.getY();
            toolPane.setFillPickerAction(null);
            toolPane.setStrokePickerAction(null);
            toolPane.setStrokeSizeAction(null);
            if (toolPane.rectBtnSelected()) {
                initRec(); //Calls method to draw rectangle
            }
            else if (toolPane.ellBtnSelected()) {
                initEll();  // Calls method to draw an ellipse
            }
            else if (toolPane.freeBtnSelected()) {
                initPath();  // Calls method to draw a path
            }
            else if(toolPane.eraseBtnSelected()){
                drawPane.getChildren().remove(event.getTarget()); // Erases shape clicked
            }
            else if(toolPane.editBtnSelected()){
                if(event.getTarget() instanceof Shape ) {
                    Shape shape = (Shape)event.getTarget();
                    initEdit(shape);  // Calls method to edit shape (drag and drop, change color and stroke)
                }
            }

        });
        primaryStage.show();

    }


    /**
     *  Method to draw rectangles on pane.
     */
    private void initRec(){
        //Creates new rectangle
        Rectangle rec = new Rectangle();
        // Saves the values of the origin
        double firstX=x;
        double firstY=y;
        // Setting translate values
        rec.setTranslateX(firstX);
        rec.setTranslateY(firstY);

        // event when mouse dragged
        drawPane.setOnMouseDragged(event -> {
            //Sets fill and stroke value and width with color picker values
            rec.setStroke(toolPane.getStrokePickerValue());
            rec.setStrokeWidth(toolPane.getStrokeSizeValue());
            rec.setFill(toolPane.getFillPickerValue());
            // getting the offset
            x=event.getX()-rec.getTranslateX();
            y=event.getY()-rec.getTranslateY();

            // Handling different cases for drawing a rectangle (different ways it can be created)
            if(event.getX()<firstX){
                rec.setTranslateX(event.getX());
                rec.setWidth(firstX-event.getX());
            }
            else{
                rec.setWidth(x);
            }
            if(event.getY()<firstY){
                rec.setHeight(firstY-event.getY());
                rec.setTranslateY(event.getY());
            }
            else{
                rec.setHeight(y);
            }
        });
        drawPane.getChildren().add(rec); // Adds shape to the drawing pane.
    }


    /**
     * Method to draw ellipses on pane
     */
    private void initEll(){
        //Creates new ellipse
        Ellipse ellipse = new Ellipse();
        //Setting translate values
        ellipse.setTranslateX(x);
        ellipse.setTranslateY(y);

        //Handles event when mouse is dragged
        drawPane.setOnMouseDragged(event -> {

            //Sets fill and stroke value and width with color picker values
            ellipse.setStroke(toolPane.getStrokePickerValue());
            ellipse.setFill(toolPane.getFillPickerValue());
            ellipse.setStrokeWidth(toolPane.getStrokeSizeValue());
            //getting offsets
            x=event.getX()-ellipse.getTranslateX();
            y=event.getY()-ellipse.getTranslateY();

            // Handling different cases for drawing a ellipse (different ways it can be created)
            if(x<0){
                ellipse.setRadiusX(-x);
            }
            else {
                ellipse.setRadiusX(x);
            }
            if(y<0){
                ellipse.setRadiusY(-y);
            }
            else{
                ellipse.setRadiusY(y);
            }
        });
        drawPane.getChildren().add(ellipse); //Adding shape to the pane.
    }


    /**
     * Method to edit a shape ( move, change fill and stroke color
     * @param shape Shape to be edited
     */
    private void initEdit(Shape shape){
        //Getting origin of the shape to move
        double firstX=x-shape.getTranslateX();
        double firstY=y-shape.getTranslateY();

        // Shape brought to the front of the pane
        shape.toFront();

        //Changing colors to whatever color is in the picker
        toolPane.setFillPickerAction(event1 -> {
            if(shape instanceof Ellipse || shape instanceof Rectangle){
                shape.setFill(toolPane.getFillPickerValue());
            }
        });
        toolPane.setStrokePickerAction(event1 -> {
            shape.setStroke(toolPane.getStrokePickerValue());
        });
        toolPane.setStrokeSizeAction(event1 -> {
            shape.setStrokeWidth(toolPane.getStrokeSizeValue());
        });
        if(toolPane.editBtnSelected()) {
            toolPane.setFillPickerValue((Color) shape.getFill());
            toolPane.setStrokePickerValue((Color) shape.getStroke());
            toolPane.setStrokeSizeValue((int) shape.getStrokeWidth());
        }



        //Shape moves when dragged
        drawPane.setOnMouseDragged(event -> {
            x=event.getX()-firstX;
            y=event.getY()-firstY;
            shape.setTranslateX(x);
            shape.setTranslateY(y);
        });

    }


    /**
     * Method to create a path (draw freely)
     */
    private void initPath(){
        // New path created and added to the pane

        Path path = new Path();
        drawPane.getChildren().add(path);

        //Stroke color and width are set
        path.setStrokeWidth(toolPane.getStrokeSizeValue());
        path.setStroke(toolPane.getStrokePickerValue());
        path.getElements().add(new MoveTo(x,y));

        // Draw as mouse is dragged.
        drawPane.setOnMouseDragged(event-> {
            path.getElements().add(new LineTo(event.getX(), event.getY()));
        });

    }

    public static void main(String[] args) {
        launch(args);
    }
}
